/*
    To be able to create routes from another file to be used in our application, from here, we have to import express as well, however, we will now use another method from express to contain our routes.

    the Router() method, will allow us to contain our routes
*/

const express = require("express");
const router = express.Router();



const courseControllers = require("../controllers/courseControllers");

const auth = require("../auth"); //import
const { verify,verifyAdmin } = auth;



//gets all course documents whether it is active or inactive:
router.get('/',verify,verifyAdmin,courseControllers.getAllCourses);

router.post('/',verify,verifyAdmin,courseControllers.addCourse);


//get all active courses for (regular, non logged-in)
router.get('/activeCourses',courseControllers.getActiveCourses);


router.post('/getSingleCourse',courseControllers.getSingleCourse);

module.exports = router;
